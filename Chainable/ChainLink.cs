using System;
using System.Collections.Generic;
using Chainable.Exceptions;

namespace Chainable
{
    public abstract class BaseChainLink
    {
        public int Index { get; protected set; }
        public ChainSettings Settings { get; protected set; }
        public List<ChainOperationLog> OperationLogs { get; protected set; }

        public void TraceOutput(string message, params object[] formatVals)
        {
            TraceOutput(String.Format(message, formatVals));
        }

        public void TraceOutput(string message)
        {
            if (Settings.TraceEnabled)
            {
                OperationLogs.Add(new ChainOperationLog
                {
                    Message = String.Format("[TRACE]\tOperation Index {0}\t{1}", Index, message),
                    OperationIndex = Index,
                    Status = ChainOperationStatus.Trace
                });
            }
        }

        public BaseChainLink(int index, ChainSettings settings)
        {
            Index = index;
            Settings = settings;
            OperationLogs = new List<ChainOperationLog>();
        }
    }

    public class ChainLink<TIn, TOut> : BaseChainLink, IChainLink
    {
        protected Func<TIn, TOut> Operation { get; set; }
        public ChainLink(int index, Func<TIn, TOut> operation, ChainSettings settings) : base(index, settings)
        {
            Operation = operation;
        }

        public object Process(object input)
        {
            try
            {
                TraceOutput("Starting to process link");
                if ((default(TIn) == null && input == null) || input is TIn)
                {
                    TraceOutput("Input valid, invoking");
                    var response = Operation((TIn) input);
                    TraceOutput("Executed, returning response");
                    return response;
                }

                TraceOutput("Type mismatch, Func was not called. Throwing exception.");
                throw new IncorrectTypeException(typeof (TIn), input == null ? null : input.GetType(), Index);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                TraceOutput("Finished processing link");
            }
        }
    }
}