﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chainable.Exceptions
{
    public class IncorrectTypeException : Exception
    {
        private string _message;
        public override string Message
        {
            get { return _message; }
        }

        public IncorrectTypeException(Type expectedType, Type foundType) : this(expectedType, foundType, null) { }

        public IncorrectTypeException(Type expectedType, Type foundType, int? index = null)
        {
            string foundTypeName;
            if (foundType == null)
            {
                foundTypeName = "NULL Reference on a NOT NULLABLE Type";
            }
            else
            {
                foundTypeName = foundType.FullName;
            }

            if (index.HasValue)
            {
                _message = String.Format("In Operation Index {0}: Expected a type of \"{1}\" but found type \"{2}\"",
                    index, expectedType.FullName, foundTypeName);
            }
            else
            {
                _message = String.Format("Expected a type of \"{0}\" but found type \"{1}\"",
                    expectedType.FullName, foundTypeName);
            }
        }
    }
}
