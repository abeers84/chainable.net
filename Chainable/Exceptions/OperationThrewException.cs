using System;

namespace Chainable.Exceptions
{
    public class OperationThrewException : Exception
    {
        public OperationThrewException(int index, Exception ex) : base("Operation Index " + index + " threw an exception, check the inner exception for details", ex) { }
    }
}