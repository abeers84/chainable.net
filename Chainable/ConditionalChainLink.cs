using System;
using System.Collections.Generic;
using Chainable.Exceptions;

namespace Chainable
{
    public class ConditionalChainLink<TIn, TOut> : BaseChainLink, IChainLink
    {
        protected Func<TIn, TOut> TrueOperation { get; set; }
        protected Func<TIn, TOut> FalseOperation { get; set; }
        protected Predicate<TIn> Predicate { get; set; }

        public ConditionalChainLink(int index, Predicate<TIn> predicate, Func<TIn, TOut> trueOperation,
            Func<TIn, TOut> falseOperation, ChainSettings settings) : base(index, settings)
        {
            TrueOperation = trueOperation;
            FalseOperation = falseOperation;
            Predicate = predicate;
        }

        public object Process(object input)
        {
            try
            {
                TraceOutput("Starting to process conditional link");
                if ((default(TIn) == null && input == null) || input is TIn)
                {
                    var arg = (TIn) input;
                    TraceOutput("Input valid, processing predicate");
                    if (Predicate(arg))
                    {
                        TraceOutput("Executing True Operation");
                        return TrueOperation(arg);
                    }
                    else if (FalseOperation != null)
                    {
                        TraceOutput("Executing False Operation");
                        return FalseOperation(arg);
                    }
                    else
                    {
                        TraceOutput("False Operation was null, not executing any operation.");
                        if ((default(TOut) == null && input == null) || input is TOut)
                        {
                            return (TOut) input;
                        }
                    }
                }
                throw new IncorrectTypeException(typeof (TIn), input == null ? null : input.GetType(), Index);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                TraceOutput("Finished processing conditional link");
            }
        }
    }
}