﻿namespace Chainable
{
    public class ChainSettings
    {
        public bool TraceEnabled { get; set; }
        public bool BreakOnError { get; set; }
        public bool EnablePerformanceDiagnostics { get; set; }
        public bool RethrowExceptions { get; set; }

        public ChainSettings()
        {
            TraceEnabled = false;
            BreakOnError = true;
            EnablePerformanceDiagnostics = true;
            RethrowExceptions = false;
        }
    }
}