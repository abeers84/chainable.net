using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Chainable.Exceptions;

namespace Chainable
{
    /// <summary>
    /// Allows simple chaining of methods with additional capabilities for each call.
    /// Allows performance tracing for each method.
    /// Additional error handling and statistics on each call.
    /// Configurable diagnostic settings.
    /// </summary>
    /// <typeparam name="TIn">The initial input type</typeparam>
    /// <typeparam name="TOut">The final resulting output type</typeparam>
    public class Chain<TIn, TOut>
    {
        public ChainSettings DefaultSettings { get; protected set; }
        protected List< IChainLink> Operations { get; set; }

        public List<ChainOperationLog> OperationLogs { get; private set; }

        public Chain()
        {
            DefaultSettings = new ChainSettings();
            Operations = new List<IChainLink>();
            OperationLogs = new List<ChainOperationLog>();
        }

        public Chain(ChainSettings defaultSettings)
            : this()
        {
            DefaultSettings = defaultSettings;
        }

        public Chain<TIn, TOut> SetDefaultSettings(ChainSettings defaultSettings)
        {
            DefaultSettings = defaultSettings;
            return this;
        }

        /// <summary>
        /// Provides simple branching in which link to execute
        /// </summary>
        /// <typeparam name="TMethodIn">The input type</typeparam>
        /// <typeparam name="TMethodOut">The output type</typeparam>
        /// <param name="method">The operation to execute</param>
        /// <param name="settings">Optional settings, will default to settings of chain if null</param>
        /// <returns>The output after calling the operation</returns>
        public Chain<TIn, TOut> Then<TMethodIn, TMethodOut>(Func<TMethodIn, TMethodOut> method, ChainSettings settings = null)
        {
            Operations.Add(new ChainLink<TMethodIn, TMethodOut>(Operations.Count(), method, settings ?? DefaultSettings));
            return this;
        }

        /// <summary>
        /// Provides simple branching in which link to execute
        /// </summary>
        /// <typeparam name="TMethodIn">The input type</typeparam>
        /// <typeparam name="TMethodOut">The output type</typeparam>
        /// <param name="predicate">The predicate which will take the input and evaluate the predicate</param>
        /// <param name="whenTrue">The operation to execute when true</param>
        /// <param name="whenFalse">The operation to execute when false</param>
        /// <param name="settings">Optional settings, will default to settings of chain if null</param>
        /// <returns>The output after calling the operation</returns>
        public Chain<TIn, TOut> When<TMethodIn, TMethodOut>(Predicate<TMethodIn> predicate,
            Func<TMethodIn, TMethodOut> whenTrue, Func<TMethodIn, TMethodOut> whenFalse, ChainSettings settings = null)
        {
            Operations.Add(new ConditionalChainLink<TMethodIn,TMethodOut>(Operations.Count(), predicate, whenTrue, whenFalse, settings ?? DefaultSettings));
            return this;
        }

        /// <summary>
        /// Provides simple branching in which link to execute
        /// </summary>
        /// <typeparam name="TMethodIn">The input type</typeparam>
        /// <typeparam name="TMethodOut">The output type</typeparam>
        /// <param name="predicate">The predicate which will take the input and evaluate the predicate</param>
        /// <param name="whenTrue">The operation to execute when true</param>
        /// <param name="settings">Optional settings, will default to settings of chain if null</param>
        /// <returns>The output after calling the operation</returns>
        public Chain<TIn, TOut> When<TMethodIn, TMethodOut>(Predicate<TMethodIn> predicate,
            Func<TMethodIn, TMethodOut> whenTrue, ChainSettings settings = null)
        {
            Operations.Add(new ConditionalChainLink<TMethodIn, TMethodOut>(Operations.Count(), predicate, whenTrue, null, settings ?? DefaultSettings));
            return this;
        }

        /// <summary>
        /// Processes all links in the chain with the specified settings
        /// </summary>
        /// <param name="input">The initial input argument which will be passed into the first link in the chain.</param>
        /// <returns>The final output after all links have been processed</returns>
        public TOut Execute(TIn input)
        {
            Stopwatch stopWatch = new Stopwatch();
            object arg = input;
            var response = default(TOut);
            foreach (var op in Operations.OrderBy(x => x.Index))
            {
                Exception thrownException = null;
                bool successful = true;
                if (op.Settings.EnablePerformanceDiagnostics)
                {
                    stopWatch.Reset();
                    stopWatch.Start();
                }

                try
                {
                    arg = op.Process(arg);
                }
                catch (Exception ex)
                {
                    thrownException = ex;
                    successful = false;
                    OperationLogs.Add(new ChainOperationLog
                    {
                        ExecutionTime = stopWatch.Elapsed,
                        OperationIndex = op.Index,
                        Status = ChainOperationStatus.Failed,
                        ThrownException = new OperationThrewException(op.Index, thrownException)
                    });
                }

                OperationLogs.AddRange(op.OperationLogs);
                    
                if (op.Settings.EnablePerformanceDiagnostics)
                {
                    stopWatch.Stop();
                    OperationLogs.Add(new ChainOperationLog
                    {
                        ExecutionTime = stopWatch.Elapsed,
                        OperationIndex = op.Index,
                        Status = ChainOperationStatus.Performance
                    });
                }

                if (!successful && thrownException != null && op.Settings.RethrowExceptions)
                {
                    throw thrownException;
                }

                if (!successful && op.Settings.BreakOnError)
                {
                    return default(TOut);
                }

                if (successful)
                {
                    OperationLogs.Add(new ChainOperationLog
                    {
                        OperationIndex = op.Index,
                        Status = ChainOperationStatus.Successful,
                        Message = String.Format("[TRACE]\tOperation Index {0}\tOperation completed successfully, continuing chain.", op.Index)
                    });
                }
            }

            if ((default(TOut) == null && arg == null) || arg is TOut)
            {
                response = (TOut) arg;
            }
            else
            {
                Exception ex = new IncorrectTypeException(typeof (TOut), arg == null ? null : arg.GetType());
                OperationLogs.Add(new ChainOperationLog
                {
                    OperationIndex = -1,
                    Status = ChainOperationStatus.Failed,
                    ThrownException = ex
                });

                if (DefaultSettings.RethrowExceptions)
                {
                    throw ex;
                }
            }

            return response;
        }
    }
}