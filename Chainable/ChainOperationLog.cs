using System;

namespace Chainable
{
    [Flags]
    public enum ChainOperationStatus
    {
        Failed = 1,
        Trace = 2,
        Performance = 4,
        Successful = 8
    }

    public class ChainOperationLog
    {
        public ChainOperationStatus Status { get; set; }
        public int OperationIndex { get; set; }
        public Exception ThrownException { get; set; }
        public TimeSpan? ExecutionTime { get; set; }
        public DateTime Occurred { get; set; }
        public string Message { get; set; }

        public ChainOperationLog()
        {
            Occurred = DateTime.Now;
        }
    }
}