﻿using System.Collections.Generic;

namespace Chainable
{
    public interface IChainLink
    {
        int Index { get; }
        ChainSettings Settings { get; }
        List<ChainOperationLog> OperationLogs { get; }
        object Process(object input);
    }
}