﻿using System;
using System.Linq;
using System.Collections.Generic;
using Chainable.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chainable.Tests
{
    [TestClass]
    public class ConditionalChainLinkTests : BaseChainTestMethods
    {
        [TestMethod]
        public void EnsureTrueAndFalseConditionIsMet()
        {
            var chain = new Chain<NumbersClass, NumbersClass>(new ChainSettings { BreakOnError = false, RethrowExceptions = false, EnablePerformanceDiagnostics = true, TraceEnabled = true })
                .When<NumbersClass, NumbersClass>((x) => !x.Any() || x.Last() > 0, ProcessNegativeFixed, ProcessFixed)
                .When<NumbersClass, NumbersClass>((x) => !x.Any() || x.Last() > 0, ProcessNegativeFixed, ProcessFixed)
                .When<NumbersClass, NumbersClass>((x) => !x.Any() || x.Last() > 0, ProcessNegativeFixed, ProcessFixed);

            var result = chain.Execute(new NumbersClass());

            Assert.AreEqual(3, result.Count());
            Assert.IsTrue(result[0] < 0);
            Assert.IsTrue(result[1] > 0);
            Assert.IsTrue(result[2] < 0);
        }

        [TestMethod]
        public void EnsureNullFalseOperation()
        {
            var chain = new Chain<NumbersClass, NumbersClass>(new ChainSettings { BreakOnError = false, RethrowExceptions = false, EnablePerformanceDiagnostics = false, TraceEnabled = true })
                .Then<NumbersClass, NumbersClass>(ProcessNegativeFixed)
                .When<NumbersClass, NumbersClass>((x) => x.Last() > 0, ProcessNegativeFixed)
                .When<NumbersClass, NumbersClass>((x) => x.Last() > 0, ProcessNegativeFixed);

            var result = chain.Execute(new NumbersClass());

            Assert.AreEqual(1, result.Count());
            Assert.IsTrue(result[0] < 0);
        }
    }

    [TestClass]
    public class SimpleChainLinkTests : BaseChainTestMethods
    {
        [TestMethod]
        public void ChainDefaultSettings()
        {
            var settings = new ChainSettings
            {
                BreakOnError = true,
                EnablePerformanceDiagnostics = true,
                RethrowExceptions = true,
                TraceEnabled = true
            };

            var chain = new Chain<NumbersClass, NumbersClass>()
                .SetDefaultSettings(settings);

            Assert.AreEqual(settings, chain.DefaultSettings);
        }

        [TestMethod]
        public void EnsureTraceLoggingWorks()
        {
            var chain = new Chain<NumbersClass, NumbersClass>(new ChainSettings { EnablePerformanceDiagnostics = false, TraceEnabled = true })
                .Then<NumbersClass, NumbersClass>(Process1)
                .Then<NumbersClass, NumbersClass>(Process1)
                .Then<NumbersClass, NumbersClass>(Process1);

            var result = chain.Execute(new NumbersClass());

            Assert.IsTrue(chain.OperationLogs.Any(x => x.Status == ChainOperationStatus.Trace));
            Assert.IsTrue(chain.OperationLogs.Where(x => x.Status == ChainOperationStatus.Performance).All(x => x.ExecutionTime.HasValue && x.ExecutionTime.Value.TotalMilliseconds > 0));
        }

        [TestMethod]
        public void EnsurePerformanceLoggingWorks()
        {
            var chain = new Chain<NumbersClass, NumbersClass>(new ChainSettings {EnablePerformanceDiagnostics = true})
                .Then<NumbersClass, NumbersClass>(Process1)
                .Then<NumbersClass, NumbersClass>(Process1)
                .Then<NumbersClass, NumbersClass>(Process1);

            var result = chain.Execute(new NumbersClass());

            Assert.AreEqual(3, chain.OperationLogs.Count(x => x.ExecutionTime.HasValue));
            Assert.IsTrue(chain.OperationLogs.Where(x => x.Status == ChainOperationStatus.Performance).All(x => x.ExecutionTime.HasValue && x.ExecutionTime.Value.TotalMilliseconds > 0));
        }

        [TestMethod]
        public void MultipleSameMethod()
        {
            var chain = new Chain<NumbersClass, NumbersClass>(new ChainSettings { BreakOnError = false, RethrowExceptions = false, EnablePerformanceDiagnostics = false })
                .Then<NumbersClass, NumbersClass>(Process1)
                .Then<NumbersClass, NumbersClass>(Process1)
                .Then<NumbersClass, NumbersClass>(Process1);

            var result = chain.Execute(new NumbersClass());

            Assert.AreEqual(3, result.Count());
            Assert.IsTrue(chain.OperationLogs.All(x => x.Status == ChainOperationStatus.Successful));
        }

        [TestMethod]
        public void MultipleSameMethod100x()
        {
            var chain = new Chain<NumbersClass, NumbersClass>(new ChainSettings { BreakOnError = false, RethrowExceptions = false, EnablePerformanceDiagnostics = false });

            for (var i = 0; i < 100; i++)
            {
                chain.Then<NumbersClass, NumbersClass>(Process1);
            }

            var result = chain.Execute(new NumbersClass());

            Assert.AreEqual(100, result.Count());
            Assert.IsTrue(chain.OperationLogs.All(x => x.Status == ChainOperationStatus.Successful));
        }

        [TestMethod]
        public void MultipleDifferentMethods()
        {
            var chain = new Chain<NumbersClass, NumbersClass>(new ChainSettings { BreakOnError = false, RethrowExceptions = false, EnablePerformanceDiagnostics = false })
                .Then<NumbersClass, NumbersClass>(Process1)
                .Then<NumbersClass, NumbersClass>(Process2);

            var result = chain.Execute(new NumbersClass());

            Assert.AreEqual(2, result.Count());
            Assert.IsTrue(chain.OperationLogs.All(x => x.Status == ChainOperationStatus.Successful));
        }

        [TestMethod]
        public void ExceptionEncounteredStillProcesses()
        {
            var chain = new Chain<NumbersClass, NumbersClass>(new ChainSettings { BreakOnError = false, RethrowExceptions = false, EnablePerformanceDiagnostics = false })
                .Then<NumbersClass, NumbersClass>(ProcessNegativeFixed)
                .Then<NumbersClass, NumbersClass>(ProcessError)
                .Then<NumbersClass, NumbersClass>(ProcessFixed);

            var result = chain.Execute(new NumbersClass());

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual(2, chain.OperationLogs.Count(x => x.Status == ChainOperationStatus.Successful));
            Assert.AreEqual(1, chain.OperationLogs.Count(x => x.Status == ChainOperationStatus.Failed));
            Assert.IsTrue(chain.OperationLogs.Where(x => x.Status == ChainOperationStatus.Failed).All(x => x.OperationIndex == 1));
            Assert.AreEqual(1, chain.OperationLogs.Count(x => x.ThrownException != null && x.ThrownException is OperationThrewException && x.ThrownException.InnerException.Message == "Error in ProcessError"));
        }

        [TestMethod]
        public void BreakOnExceptionWrongOutputType()
        {
            var chain = new Chain<NumbersClass, BigNumbersClass>(new ChainSettings{ BreakOnError = true, RethrowExceptions = false, EnablePerformanceDiagnostics = false })
                .Then<NumbersClass, NumbersClass>(ProcessError)
                .Then<NumbersClass, BigNumbersClass>(ConvertToBigNumbers);

            var result = chain.Execute(new NumbersClass());

            Assert.AreEqual(default(BigNumbersClass), result);
            Assert.AreEqual(1, chain.OperationLogs.Count());
            Assert.IsTrue(chain.OperationLogs.All(x => x.Status == ChainOperationStatus.Failed));
        }

        [TestMethod]
        public void WrongOutputTypeNotNullable()
        {
            var chain = new Chain<NumbersClass, int>(new ChainSettings { BreakOnError = false, RethrowExceptions = false, EnablePerformanceDiagnostics = false })
                .Then<NumbersClass, NumbersClass>(ProcessFixed)
                .Then<NumbersClass, BigNumbersClass>(FailedConvert)
                .Then<BigNumbersClass, Int64>(BigSum);

            var result = chain.Execute(new NumbersClass());

            Assert.AreEqual(default(int), result);
            Assert.AreEqual(4, chain.OperationLogs.Count());
            Assert.AreEqual(3, chain.OperationLogs.Count(x => x.Status == ChainOperationStatus.Failed));
            Assert.AreEqual(1, chain.OperationLogs.Count(x => x.ThrownException != null && x.ThrownException is IncorrectTypeException));
            Assert.AreEqual(2, chain.OperationLogs.Count(x => x.ThrownException != null && x.ThrownException is OperationThrewException));
            Assert.AreEqual(1, chain.OperationLogs.Count(x => x.ThrownException != null && x.ThrownException is OperationThrewException && x.ThrownException.InnerException is IncorrectTypeException));
            Assert.IsNotNull(chain.OperationLogs.Last().ThrownException);
            Assert.AreEqual(-1, chain.OperationLogs.Last().OperationIndex);
        }
    }

    public abstract class BaseChainTestMethods
    {
        protected NumbersClass Process1(NumbersClass input)
        {
            input.Add(DateTime.Now.Second);
            return input;
        }

        protected NumbersClass Process2(NumbersClass input)
        {
            input.Add(-1 * DateTime.Now.Second);
            return input;
        }

        public NumbersClass ProcessNegativeFixed(NumbersClass input)
        {
            input.Add(-2782);
            return input;
        }

        public NumbersClass ProcessFixed(NumbersClass input)
        {
            input.Add(2782);
            return input;
        }

        public NumbersClass ProcessError(NumbersClass input)
        {
            throw new Exception("Error in ProcessError");
        }

        public BigNumbersClass ConvertToBigNumbers(NumbersClass input)
        {
            var response = new BigNumbersClass();
            response.AddRange(input.Select(x => (Int64)x).ToArray());
            return response;
        }

        public BigNumbersClass FailedConvert(NumbersClass input)
        {
            throw new Exception("Error in FailedConvert");
        }

        public int Sum(NumbersClass input)
        {
            return input.Sum();
        }

        public Int64 BigSum(BigNumbersClass input)
        {
            return input.Sum();
        }
    }

    public class NumbersClass : List<Int32> { }

    public class BigNumbersClass : List<Int64> { }
}
